var request = require('request');
var _ = require('lodash');

var auditIp = process.env.MSVCAUDIT_PORT_3000_TCP_ADDR || 'localhost';
var auditPort = process.env.MSVCAUDIT_PORT_3000_TCP_PORT ||'3006';


function numericLogLevel(level) {
  switch (level){
    case 'all':
      return 0;
    case 'info':
      return 1;
    case 'change':
      return 2;
    case 'match':
      return 3;
    default:
      return 1;//default to info
  }
}


//TODO: would this be better as another good-reporter with it's own event?
module.exports = {
  events : [],
  options : {},
  debug : false,
  loggingLevel : numericLogLevel('all'),
  loggingEndpoint : 'http://' + auditIp + ':' + auditPort + "/",
  loggingEnabled : true,
  applyPolicy : function(policy){
    if (!policy.audit){
      console.dir(policy)
    };
    this.loggingEnabled = policy.audit.audit_trail !== 'none';
    this.loggingLevel = numericLogLevel(policy.audit.loglevel);
    if (policy.audit.audit_trail === 'external') {
      this.loggingEndpoint = policy.audit.external_endpoint;

    }
  },
  configure : function (request, service, transactionId, token, claims){
    this.options = {
      //Who
      customerId      : request.params.customer_id,
      userId          : claims.prn,

      //How
      appId           : claims.iss,
      service         : service,
      sourceIp        : request.headers['X-Real-IP'] || request.headers['x-forwarded-for'] || request.info.remoteAddress,
      sourceKey       : request.headers['tf_key'], //TODO: formalise
      serviceIp       : request.server.info.address + ':' + request.server.info.port,

      //When
      datetime        : undefined,
      timestamp       : undefined,

      //What
      transactionId   : transactionId,
      sequenceId      : undefined,
      action          : undefined,
      specifier       : undefined,
      summary         : undefined,
      details         : undefined,
      loglevel        : 'info',
      status          : undefined

    };

    this.headers = {
        'Authorization': token
    };
    this.loggingEndpoint = 'http://' + auditIp + ':' + auditPort + "/" + this.options.customerId;
  },
  logAndSend : function (action, summary, specifier, details, status, loglevel){
    this.log(action, summary, specifier, details, status, loglevel);
    this.send();
  },
  log : function (action, summary, specifier, details, status, loglevel){
    var event = _.clone(this.options);
    _.assign(event, {
      datetime : (new Date()).toISOString(),
      timestamp : Date.now(),
      action : action,
      summary : summary,
      specifier : specifier,
      details : _.flatten([details]),
      status : status
      });
    if (loglevel != undefined){
      event.loglevel = loglevel;
    }
    if (this.loggingLevel <= numericLogLevel(event.loglevel)){
      this.events.push(event);
    }
  },
  send : function (){
      var self = this;
    if (this.debug){
      for (var i =0; i < this.events.length; i++){
        console.log(this.events[i]);
      }
      return;
    }
    if (this.events.length > 0 && this.loggingEnabled){
      request(
        {
          method : 'POST',
          uri : this.loggingEndpoint,
          json : true,
          headers : this.headers,
          body : this.events
        },
        function (err,req,res) { //TODO: fire and forget
            self.events = [];
            if (err) {
                console.error(err);
            }

            else if(res){
                if(res.statusCode && res.statusCode / 100 !== 2){
                    if(res.statusCode == 401){
                        console.log("Authorization failed for logging to audit trail")
                    }
                    else{
                        console.log("Error logging to audit trail. StatusCode: " + res.statusCode)
                    }

                }

                if (!res.success) {
                    console.log("Error logging to audit trail.")
                    console.dir(res);
                }
            }
        }
      )
    }
  }
}
