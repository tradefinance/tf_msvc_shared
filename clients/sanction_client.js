var request = require('request');
var _ = require('lodash');

var Ip = process.env.MSVCSANCTIONS_PORT_3000_TCP_ADDR || 'localhost';
var Port = process.env.MSVCSANCTIONS_PORT_3000_TCP_PORT ||'3001';

module.exports = {
  location : function (customerId, token, transactionId, sequenceId, location, callback){
    request(
      {
        method : 'POST',
        uri : 'http://' + Ip + ':' + Port + "/location/" + customerId,
        json : true,
        headers : {
            'Authorization' : token
        },
        body : {
          transactionId : transactionId,
          sequenceId : sequenceId,
          port : location.port,
          city : location.city,
          countryName : location.countryName,
          isoCode : location.isoCode
        }
      },
      function (err,req,res) {
          if (err) {
              return callback(err,null);
          }

          if(res && res.statusCode && res.statusCode / 100 !== 2){
              return callback(res, null)
          }

        return callback(null,res);
      }
    )
  }
}
