var request = require('request');
var _ = require('lodash');

var policyIp = process.env.MSVCPOLICY_PORT_3000_TCP_ADDR || 'localhost';
var policyPort = process.env.MSVCPOLICY_PORT_3000_TCP_PORT ||'3003';

module.exports = {
  RetrievePolicy : function (customerId, token, callback){
    request(
      {
        method : 'GET',
        uri : 'http://' + policyIp + ':' + policyPort + "/" + customerId + "/latest",
        json : true,
        headers : {
          'Authorization' : token
        }
      },
      function (err,req,res) {
          if (err) {
              return callback(err,null);
          }

          if(res && res.statusCode && res.statusCode / 100 !== 2){
              return callback(res, null)
          }

        return callback(null,res);
      }
    )
  }
}
