var request = require('request');
var _ = require('lodash');

var Ip = process.env.MSVCGEO_PORT_3000_TCP_ADDR || 'localhost';
var Port = process.env.MSVCGEO_PORT_3000_TCP_PORT ||'3002';

module.exports = {
  AutoCorrect : function (customerId, token, transactionId, sequenceId, location, iso, callback){
    request(
      {
        method : 'POST',
        uri : 'http://' + Ip + ':' + Port + "/autocorrect/" + customerId,
        json : true,
          headers : {
              'Authorization' : token
          },
        body : {
          transactionId : transactionId,
          sequenceId : sequenceId,
          location : location,
          iso : iso
        }
      },
      function (err,req,res) {
          if (err) {
              return callback(err,null);
          }

          if(res && res.statusCode && res.statusCode / 100 !== 2){
              return callback(res, null)
          }

        return callback(null,res);
      }
    )
  }
}
