var request = require('request');
var _ = require('lodash');

var Ip = process.env.MSVCDUG_PORT_3000_TCP_ADDR || 'localhost';
var Port = process.env.MSVCDUG_PORT_3000_TCP_PORT ||'3004';

module.exports = {
  Screen : function (customerId, token, transactionId, sequenceId, goodsDescription, options, callback){
    var body = {
      transactionId : transactionId,
      sequenceId : sequenceId,
      goodsDescription : goodsDescription
    };
    if (options && options.all_results){
      body.all_results = options.all_results;
    }
    if (options && options.individualWords !== null){
      body.individualWords = options.individualWords;
    }
    request(
      {
        method : 'POST',
        uri : 'http://' + Ip + ':' + Port + "/" + customerId,
        json : true,
          headers : {
              'Authorization' : token
          },
        body : body
      },
      function (err,req,res) {
          if (err) {
              return callback(err,null);
          }

          if(res && res.statusCode && res.statusCode / 100 !== 2){
              return callback(res, null)
          }

        return callback(null,res);
      }
    )
  }
}
