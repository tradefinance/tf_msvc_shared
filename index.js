module.exports = {
  audit_client : require('./clients/audit_client'),
  policy_client : require('./clients/policy_client'),
  geo_client : require('./clients/geo_client'),
  sanction_client : require('./clients/sanction_client'),
  dug_client : require('./clients/dug_client'),
}
